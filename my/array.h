#include <cstddef>
#include <algorithm>
#include <iterator>

namespace my
{

template<typename T, std::size_t N>
struct inner_array
{
    T data[N];
    T       *ptr()       {return data;}
    T const *ptr() const {return data;}
};
template<typename T>
struct inner_array<T, 0>
{
    T       *ptr()       {return nullptr;}
    T const *ptr() const {return nullptr;}
};

template<typename T, std::size_t N>
struct array
{
    inner_array<T, N> inner;

    using      value_type = T;
    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       reference = value_type       &;
    using const_reference = value_type const &;

    using         pointer = value_type       *;
    using   const_pointer = value_type const *;

    using        iterator = value_type       *;
    using  const_iterator = value_type const *;

    using       reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

          pointer data()       {return inner.ptr();}
    const_pointer data() const {return inner.ptr();}

          reference operator[](size_type const i)       {return data()[i];}
    const_reference operator[](size_type const i) const {return data()[i];}

    bool     empty() const {return N == 0;}
    size_type size() const {return N;}
    
    void fill(T const &value)
    {
        for(T &x : *this)
            x = value;
    }

    iterator begin() {return data();}
    iterator   end() {return data() + N;}

    const_iterator begin() const {return data();}
    const_iterator   end() const {return data() + N;}

    reverse_iterator rbegin() {return reverse_iterator(  end());}
    reverse_iterator   rend() {return reverse_iterator(begin());}
    const_reverse_iterator rbegin() const {return const_reverse_iterator(  end());}
    const_reverse_iterator   rend() const {return const_reverse_iterator(begin());}

          reference front()       {return data()[0];}
    const_reference front() const {return data()[0];}
          reference  back()       {return data()[N - 1];}
    const_reference  back() const {return data()[N - 1];}
};

template<typename T, std::size_t N>
bool operator<(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
{
    return std::lexicographical_compare
    (
        lhs.begin(), lhs.end(),
        rhs.begin(), rhs.end()
    );
}
template<typename T, std::size_t N>
bool operator>(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
{
    return std::lexicographical_compare
    (
        rhs.begin(), rhs.end(),
        lhs.begin(), lhs.end()
    );
}
template<typename T, std::size_t N>
bool operator==(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
{
    return std::equal(lhs.begin(), lhs.end(), rhs.begin());
}
template<typename T, std::size_t N>
bool operator!=(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
{
    return !(lhs == rhs);
}
template<typename T, std::size_t N>
bool operator>=(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
{
    return !(lhs < rhs);
}
template<typename T, std::size_t N>
bool operator<=(my::array<T, N> const &lhs, my::array<T, N> const &rhs)
{
    return !(lhs > rhs);
}

template<std::size_t I, typename T, std::size_t N>
T        &get(my::array<T, N>        &a) {return a[I];}
template<std::size_t I, typename T, std::size_t N>
T const  &get(my::array<T, N> const  &a) {return a[I];}
template<std::size_t I, typename T, std::size_t N>
T       &&get(my::array<T, N>       &&a) {return a[I];}
template<std::size_t I, typename T, std::size_t N>
T const &&get(my::array<T, N> const &&a) {return a[I];}

} // namespace my

namespace std
{

template<typename T, std::size_t N>
struct tuple_size<my::array<T, N>>
{
    static constexpr std::size_t value = N;
};
template<std::size_t I, typename T, std::size_t N>
struct tuple_element<I, my::array<T, N>>
{
    using type = T;
};

} // namespace std
